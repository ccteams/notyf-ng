import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notyf-ng',
  templateUrl: './notyf.component.html',
  styleUrls: ['./notyf.component.scss']
})
export class NotyfComponent implements OnInit {

  private messageArr = [];

  constructor() { }

  ngOnInit() {
  }

  addNotice(message :string, type :string="info", opt? :any) {
    const t = this;
    if(!message) return;
    let obj = {type,message};
    t.messageArr.push(obj);
    setTimeout(()=> {
      let idx = t.messageArr.indexOf(obj);
      if(idx === -1) return;
      t.messageArr.splice(idx,1);
    }, type === "info" ? 2000 : 3500);
  }

}
