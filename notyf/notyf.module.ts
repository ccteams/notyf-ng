import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotyfComponent } from "./notyf.component";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    NotyfComponent
  ],
  exports: [
    NotyfComponent
  ]
})
export class NotyfModule { }
