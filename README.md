### Angular2 prompt message
```
	<app-notyf-ng #notyf></app-notyf-ng>
	
	@NgModule({
	  imports: [NotyfModule],
	  bootstrap: [AppComponent]
	})
	export class AppModule { }
	
	@ViewChild("notyf")
	private notyf :NotyfComponent;
	
	this.notyf.addNotice("Hello World!!", "info");
	this.notyf.addNotice("Hello World!!", "error");
```